package fk;

import user_data.RewardItem;
import quests.QuestReward;
import fk.ChangeHat;
import etwin.Obfu;
import hf.mode.GameMode;

class QuestCalnoel implements QuestReward {

  public function new() {}

  public function give(game: GameMode, reward: RewardItem): Void {
    fk.ChangeHat.calnoel = true;
  }

  public function remove(game: GameMode, reward: RewardItem): Void {
  }
}
