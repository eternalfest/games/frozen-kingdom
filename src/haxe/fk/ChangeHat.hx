package fk;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.Mode;
import hf.entity.Player;
import hf.GameManager;
import hf.mode.GameMode;
import keyboard.Key;
import keyboard.KeyCode;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;
import merlin.Merlin;

@:build(patchman.Build.di())
class ChangeHat {

	public static var calnoel: Bool = false;

	@:diExport
	public var patch(default, null): IPatch;

	public function new(): Void {
    	this.patch = new PatchList([
      		Ref.auto(GameMode.getControls).wrap(function(hf: Hf, self: GameMode, old): Void {

      			var hf: Hf = self.root;

        		if (self.fl_disguise && Key.isDown(KeyCode.D) && self.keyLock != 68) {

					var player = (self.getPlayerList())[0];
					var v4 = player.head;

					if (player.head == hf.Data.HEAD_TUB) {
				        if(calnoel) {
			        		player.head = 13;
			        	} else {
			        		player.head = 1;
			        	}
				    }
					else if (player.head == 13) {
			        	player.head = 1;
					}
					else {
						player.head++;
					}

					if (v4 != player.head) {
				    	player.replayAnim();
				    }
					self.keyLock = 68;
				}

				old(self);
     		}),
			
			Ref.auto(GameMode.initPlayer).wrap(function(hf: Hf, self: GameMode, player: Player, old): Void {
				if(calnoel) {
                    player.head = 13;
				}
                old(self, player);
			}),

    	]);
  	}
}
