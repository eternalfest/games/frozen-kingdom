package user_data;
import hf.mode.GameMode;
class Inventory {
    public var inventory: Map<Int, Int>;
    public var game: GameMode;
    public var unlock: Map<Int, Int>;
    public function new(i: Map<Int, Int>, game: GameMode) {
        inventory = i;
        this.game = game;
        unlock = new Map<Int, Int>();
        for(i in inventory.keys())
            unlock[i] = 10 - inventory[i];
    }

    public function get_keys(): Array<Int> {
        var array = new Array<Int>();
        for(j in inventory.keys()) {
            array.push(j);
        }
        return array;
    }

    public function get_score_keys(): Array<Int> {
        var array = new Array<Int>();
        for(j in inventory.keys()) {
            if(j >= 1000)
                array.push(j);
        }
        for(j in 0...game.scorePicks.length)
            if(array.indexOf(1000 + j) == -1 && game.scorePicks[j] != null)
                array.push(1000 + j);
        return array;
    }

    public function get_special_keys(): Array<Int> {
        var array = new Array<Int>();
        for(j in inventory.keys()) {
            if(j < 1000)
                array.push(j);
        }
        for(j in 0...game.specialPicks.length)
            if(array.indexOf(j) == -1 && game.specialPicks[j] != null)
                array.push(j);
        return array;
    }

    public function get(id: Int): Int {
        var picked:Int = 0;
        if(id >= 1000 && game.scorePicks[id - 1000] != null)
            picked += game.scorePicks[id - 1000];
        else if(game.specialPicks[id] != null)
            picked += game.specialPicks[id];
        return (inventory[id] == null ? 0 : inventory[id]) + picked;
    }

    public function add_item(id: Int): Bool {
        if(unlock[id] == null)
            unlock[id] = 10;
        unlock[id] = unlock[id] - 1;
        return unlock[id] == 0;
    }
}
