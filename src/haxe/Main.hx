import bugfix.Bugfix;
import debug.Debug;
import fk.QuestCalnoel;
import fk.ChangeHat;
import noel.Ice;
import noel.Actions;
import user_data.UserData;
import quests.QuestManager;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;
import better_script.BetterScript;
import etwin.Obfu;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	changeHat: ChangeHat,
    atlasBoss: atlas.props.Boss,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
	actions: Actions,
    game_params: GameParams,
    betterScript: BetterScript,
	quests: QuestManager,
    merlin: Merlin,
    user_data: UserData,
    patches: Array<IPatch>,
    hf: Hf
  ) {
	quests.register_reward(Obfu.raw("calnoel"), new QuestCalnoel());
    Patchman.patchAll(patches, hf);
  }
}
