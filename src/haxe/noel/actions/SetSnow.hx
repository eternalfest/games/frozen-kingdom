package noel.actions;

import hf.Hf;
import hf.mode.GameMode;
import noel.Ice;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class SetSnow implements IAction {
  public var name(default, null): String = Obfu.raw("setSnow");
  public var isVerbose(default, null): Bool = false;

  private var ice(default, null): Ice;
    
  public function new(ice: Ice) {
    this.ice = ice;
  }

  public function run(ctx: IActionContext): Bool {
    var density: Float = ctx.getOptInt(Obfu.raw("density")).or(0);
    var speed: Float = ctx.getOptInt(Obfu.raw("speed")).or(0);

    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    ice.enableIce(hf, game, density, speed);

    return false;
  }
}
