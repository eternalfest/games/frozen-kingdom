package noel;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;

import noel.Ice;
import noel.actions.SetSnow;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var setSnow(default, null): IAction;

  public function new(ice: Ice, patches: Array<IPatch>) {
    this.patches = FrozenArray.from(patches);
    this.setSnow = new SetSnow(ice);
  }
}